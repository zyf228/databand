package com.ruoyi.web.mapper;

import java.util.List;
import com.ruoyi.web.domain.DatabandSitemenu;

/**
 * 站点菜单Mapper接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface DatabandSitemenuMapper 
{
    /**
     * 查询站点菜单
     * 
     * @param id 站点菜单ID
     * @return 站点菜单
     */
    public DatabandSitemenu selectDatabandSitemenuById(Long id);

    /**
     * 查询站点菜单列表
     * 
     * @param databandSitemenu 站点菜单
     * @return 站点菜单集合
     */
    public List<DatabandSitemenu> selectDatabandSitemenuList(DatabandSitemenu databandSitemenu);

    /**
     * 新增站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    public int insertDatabandSitemenu(DatabandSitemenu databandSitemenu);

    /**
     * 修改站点菜单
     * 
     * @param databandSitemenu 站点菜单
     * @return 结果
     */
    public int updateDatabandSitemenu(DatabandSitemenu databandSitemenu);

    /**
     * 删除站点菜单
     * 
     * @param id 站点菜单ID
     * @return 结果
     */
    public int deleteDatabandSitemenuById(Long id);

    /**
     * 批量删除站点菜单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandSitemenuByIds(String[] ids);
}
