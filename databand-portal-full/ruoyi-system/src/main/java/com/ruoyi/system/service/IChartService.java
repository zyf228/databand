package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.DyDataset;


public interface IChartService {
	
	 public List<DyDataset> selectDatasetList(String datetype,String productline,String channel);

}
