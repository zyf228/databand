package org.databandtech.api.controller;

import java.util.List;

import org.databandtech.api.entity.DatabandSite;
import org.databandtech.api.entity.core.AjaxResult;
import org.databandtech.api.service.IDatabandSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/sites")
public class SiteController {

	@Autowired
	private IDatabandSiteService databandSiteService;

	@GetMapping("/list")
    @CrossOrigin
    @ResponseBody
    public AjaxResult list()
    {        
		List<DatabandSite> list = databandSiteService.selectDatabandSiteListAll();

    	AjaxResult result = AjaxResult.success(list);
        return result;
    }

//    @GetMapping("/list")
//    @CrossOrigin
//    @ResponseBody
//    public List<DatabandSite> list()
//    {
//        List<DatabandSite> list = databandSiteService.selectDatabandSiteListAll();
//        return list;
//    }

	@GetMapping("/listPage")
	@ResponseBody
	public PageInfo<DatabandSite> listPage() {
		PageHelper.startPage(1, 10);
		List<DatabandSite> list = databandSiteService.selectDatabandSiteListAll();
		PageInfo<DatabandSite> pageInfo = new PageInfo<DatabandSite>(list);

		return pageInfo;
	}

}
