package org.databandtech.clickhouse.entity;

public class EventLog {
	
	
	String eventtime;
	int uid;
	String vid;
	String source;
	String logtype;
	String city;
	int duration;
	
	public EventLog(String eventtime, int uid, String vid, String source, String logtype, String city,
			int duration) {
		super();
		this.eventtime = eventtime;
		this.uid = uid;
		this.vid = vid;
		this.source = source;
		this.logtype = logtype;
		this.city = city;
		this.duration = duration;
	}
	
	public String getEventtime() {
		return eventtime;
	}
	public void setEventtime(String eventtime) {
		this.eventtime = eventtime;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getVid() {
		return vid;
	}
	public void setVid(String vid) {
		this.vid = vid;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getLogtype() {
		return logtype;
	}
	public void setLogtype(String logtype) {
		this.logtype = logtype;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}	
	
	@Override
	public String toString() {
		return "EventLog [eventtime=" + eventtime + ", uid=" + uid + ", vid=" + vid + ", source=" + source
				+ ", logtype=" + logtype + ", city=" + city + ", duration=" + duration + "]";
	}
}
