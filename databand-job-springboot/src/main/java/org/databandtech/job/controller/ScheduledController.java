package org.databandtech.job.controller;

import java.util.List;

import org.databandtech.job.entity.ScheduledBean;
import org.databandtech.job.service.ScheduledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScheduledController {

	@Autowired
	private ScheduledService scheduledService;

	@RequestMapping("/getAllSchedule")
	public List<ScheduledBean> getAllSchedule() {
		return scheduledService.getAllSchedule();
	}

	@RequestMapping("/start")
	public String start(@RequestParam("jobcode") String jobcode) {
		scheduledService.start(jobcode);
		return "start ok";
	}

	@RequestMapping("/stop")
	public String stop(@RequestParam("jobcode") String jobcode) {
		scheduledService.stop(jobcode);
		return "stop ok";
	}

	@RequestMapping("/restart")
	public String restart(@RequestParam("jobcode") String jobcode) {
		scheduledService.restart(jobcode);
		return "restart ok";
	}

}
