SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `databand_scheduletask`
-- ----------------------------
DROP TABLE IF EXISTS `databand_scheduletask`;
CREATE TABLE `databand_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtype` varchar(128) NOT NULL COMMENT '任务类型',
  `jobcode` varchar(128) NOT NULL COMMENT '任务关键key，任务实例id',
  `beaname` varchar(128) DEFAULT NULL COMMENT 'bean名称，只在java型任务有效',
  `methodname` varchar(128) DEFAULT NULL COMMENT '方法名称，只在java型任务有效',
  `methodparams` varchar(128) DEFAULT NULL COMMENT '方法参数，只在java型任务有效',
  `descri` varchar(128) DEFAULT NULL COMMENT '任务描述',
  `cron` varchar(128) NOT NULL COMMENT 'cron任务表达式',
  `ext` varchar(500) DEFAULT NULL COMMENT '扩展字段',
  `status` int(2) NOT NULL DEFAULT '-1' COMMENT '成功 1 是 0 否',
  `startflag` int(2) DEFAULT '0',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqu_jobcode` (`jobcode`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of databand_scheduletask
-- ----------------------------
INSERT INTO `databand_scheduletask` VALUES ('1', 'HdfsToJdbcSqoop1', 'HdfsToJdbcSqoop1Job', '-', '-', '-', '每日数据导入:产品各分类订单数', '0/45 * * * * ?', '{}', '-1', '0', '2020-12-14 10:46:37', '2020-12-17 15:58:05');
INSERT INTO `databand_scheduletask` VALUES ('6', 'cmd', 'WindowsDir1', '-', '-', '-', '命令行：dir', '0/10 * * * * ?', '{}', '-1', '0', '2020-12-16 20:50:45', '2020-12-16 20:58:53');
INSERT INTO `databand_scheduletask` VALUES ('7', 'cmd', 'WindowsIP1', '-', '-', '-', '命令行：ipconfig', '0/25 * * * * ?', '{}', '-1', '0', '2020-12-16 20:51:36', '2020-12-16 20:59:10');
INSERT INTO `databand_scheduletask` VALUES ('8', 'HdfsBackup', 'hdfs_product2020', '-', '-', '-', 'HDFS产品数据每日备份，v2020版', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-17 14:57:13', '2020-12-17 14:57:39');
INSERT INTO `databand_scheduletask` VALUES ('9', 'HdfsToLocalFile', 'hdfs_toLocal2020', '-', '-', '-', 'HDFS定时复制文件到文件系统', '0/15 * * * * ?', '{}', '-1', '0', '2020-12-17 15:44:21', '2020-12-17 15:59:59');
INSERT INTO `databand_scheduletask` VALUES ('11', 'HdfsToLocalFile', 'hdfs_toLocal2020_1', '-', '-', '-', 'HDFS定时复制文件到文件系统的另一个实例', '0/20 * * * * ?', '{}', '-1', '0', '2020-12-17 15:59:53', '2020-12-17 15:59:53');
INSERT INTO `databand_scheduletask` VALUES ('12', 'HiveSqlExecute', 'hiveSqlExecuteJob1', '-', '-', '-', 'hive执行型任务', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-21 17:53:41', '2020-12-21 17:53:41');
INSERT INTO `databand_scheduletask` VALUES ('13', 'HiveSqlQueryJob', 'hiveSqlQueryJob1', '-', '-', '-', 'hive数据查询并且sink导出', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-23 16:44:56', '2020-12-23 16:44:56');

DROP TABLE IF EXISTS `databand_fromhive`;
CREATE TABLE `databand_fromhive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buycount` int(11) DEFAULT NULL,
  `citycode` varchar(50) DEFAULT NULL,
  `saledatetime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_fromhive
-- ----------------------------
INSERT INTO `databand_fromhive` VALUES ('1', '100', '广州', '2010-10-10');